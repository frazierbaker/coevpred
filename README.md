# CoevPred

Coevolution-based metal-binding site prediction.

## Modules:
1. CoevFeat

## Installation:
```
git clone http://gitlab.com/frazierbaker/coevpred
cd coevpred
python3 -m pip install --user -e .
```

For more usage instructions, see `python3 -m coevpred.coevfeat --help`