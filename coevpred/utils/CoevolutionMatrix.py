class CoevolutionMatrix(object):
    def __init__(self, filename):
        cf = open(filename, "r")
        self.prot = filename.replace(".align7.pearson.wph","")
        aa = next(cf)
        aa = aa.split()
        seq = next(cf)
        seq = seq.split()
        aaseq = {"".join([aa[i],seq[i]]):i for i in range(len(seq))}
        self.rev_aaseq =  {i:"".join([aa[i],seq[i]]) for i in range(len(seq))}
        self.aaseq = aaseq
        self.sym_matrix = [[0 for rr in aaseq] for r in aaseq]
        self.shannon = None
        for line in cf:
            resid = "".join(line.split()[0:2])
            i = aaseq[resid]
            values = line.split()[2:]
            for j in range(len(values)):
                self.sym_matrix[i][j] = abs(float(values[j]))
                self.sym_matrix[j][i] = abs(float(values[j]))
        cf.close()
    def __getitem__(self, index):
        x,y = index
        i = self.aaseq[x]
        j = self.aaseq[y]
        return self.sym_matrix[i][j]
    def read_shannon(self, filename):
        with open(filename, "r") as f:
            self.shannon = {line.split()[0]: line.split()[1].replace("\n","") for line in f}
    def read_select(self, filename, binary=False):
        with open(filename, "r") as f:
            self.classes = {"{prot}_{res}".format(prot=self.prot, res=res): 0 for res in self.aaseq}
            metals = ["Ca","Cu","Fe","Mg","Mn","Zn"]
            for line in f:
                if ">" in line: continue
                if len(line) < 1: continue
                res,metal = line.replace("\n","").split()
                if binary:
                    metal = 1
                else:
                    metal = metals.index(metal)
                self.classes["{prot}_{res}".format(prot=self.prot, res=res)] = metal