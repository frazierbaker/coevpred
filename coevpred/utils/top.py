class ResidueRelation(object):
    def __init__(self, value, tiebreaker, label):
        self.value = abs(float(value))
        self.tiebreaker = float(tiebreaker)
        self.label = label
    def __lt__(self, other):
        if self.value < other.value:
            return True
        elif self.value == other.value:
            return self.tiebreaker < other.tiebreaker
    def __eq__(self, other):
        if self.value == other.value:
            return self.tiebreaker == other.tiebreaker
        return False
    def __str__(self):
        return "ResidueRelation<{value},{tiebreaker},{label}>".format(value=self.value, tiebreaker=self.tiebreaker, label=self.label)
    def __repr__(self):
        return str(self)

def residue_data(row, rowi, row_labels, tiebreaker):
    if len(row) == 0:
        return []
    data = [ResidueRelation(row[i], tiebreaker[i], row_labels[i]) for i in range(len(row)) if i != rowi]
    return data
    
def top_residues(row, rowi, row_labels, n, tiebreaker):
    if len(row) == 0:
        return []
    
    data = [ResidueRelation(row[i], tiebreaker[i], row_labels[i]) for i in range(len(row)) if i != rowi]
    results = top(data, n)
    return results
    
def top(data, n):
    data_sorted = sorted(data, reverse=True)
    group = data_sorted[:n]
    cutoff = group[-1]
    if len(data_sorted) > n:
        k = n
        while k < len(data_sorted):
            if data_sorted[k] < cutoff: break
            group.append(data_sorted[k])
            k+=1
    return group


###########################################################################################################################################
###########################################################################################################################################
###########################################################################################################################################
#                                               The following are temporary utils:

def top_residues_no(row, rowi, row_labels, n, tiebreaker):
    if len(row) == 0:
        return []
    
    data = [ResidueRelation(row[i], tiebreaker[i],row_labels[i]) for i in range(len(row)) if i != rowi]
    results = top_no(data, n)
    return results
    
def top_no(data, n):
    cutoff = sorted(data, reverse=True)[n-1].value
    group = [d for d in data if d.value >= cutoff]
    return group

def groupLn(row, rowi, row_labels,n, tiebreaker):
    g = top_residues_no(row, rowi, row_labels, n, tiebreaker)
    return len(g) > n
    