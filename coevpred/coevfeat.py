#!/usr/bin/env python3

import argparse
import subprocess
import json
import sys
from .utils import top_residues, residue_data, alphabet

def aggregate_weighted_mean(group, mapping, pseudo=0.0):
    counts = {m: pseudo for m in mapping}
    total = 0.0
    for x in group:
        for y in mapping:
            if x.label in mapping[y]:
                counts[y] += float(x.value)
    total = sum(list(map(lambda x: x.value, group))) + pseudo
    if total == 0.0: return counts
    for c in counts:
        counts[c] /= total
    return counts

def aggregate_weighted_lor(group, protein, mapping):
    local = aggregate_weighted_mean(group, mapping, pseudo=1.0)
    full = aggregate_weighted_mean(protein, mapping, pseudo=1.0)
    lor = {}
    for m in mapping:
        lor[m] = (local[m]) / (full[m])
    return lor

def aggregate_mean(group, mapping, pseudo=0.0):
    counts = {m: pseudo for m in mapping}
    total = pseudo + len(group)
    for x in group:
        for y in mapping:
            if x.label in mapping[y]:
                counts[y] += 1.0
    if total == 0.0: return counts
    for c in counts:
        counts[c] /= total
    return counts

def aggregate_lor(group, protein, mapping):
    local = aggregate_mean(group, mapping, pseudo=1.0)
    full = aggregate_mean(protein, mapping, pseudo=1.0)
    lor = {}
    for m in mapping:
        lor[m] = (local[m]) / (full[m])
    return lor

def aggregate_tiebreaker(group):
    count = 0.0
    total = 0.0
    for x in group:
        count += float(x.tiebreaker)
        total += 1.0
    if total == 0.0: return 0.0
    count /= total
    return count

def aggregate_weighted_tiebreaker(group):
    count = 0.0
    total = 0.0
    for x in group:
        count += float(x.value)*float(x.tiebreaker)
        total += float(x.value)
    if total == 0.0: return 0.0
    count /= total
    return count

def features(row, shannon, aa, i, n, pssm, subset, all=False):
    sz_map = {"large": "FKRWY",
                "small":"ACGPST",
                "medium":"DEHILMNQV"}
    hp_map = {"hydrophobic": "ACFILMV",
              "polar":"DEHLNQRSTWY",
              "positive":"HKR",
              "negative":"DE"}
    identity_map = {x: x for x in alphabet}
    group = top_residues(row, i, aa, n, shannon)
    protein = residue_data(row, i, aa, shannon)
    central_residue = aa[i]
    results = {}
    results.update({"central."+x: int(central_residue==x) for x in subset}) # Central One Hot
    results.update({"shannon": shannon[i], "indivp": row[i]}) # Individual Probability and Shannon
    results.update({"pssm."+x: pssm["Score"][x] for x in alphabet}) # Individual PSSM Data
    
    waa_perc = aggregate_weighted_mean(group, identity_map)
    results.update({"waa_perc."+x: waa_perc[x] for x in alphabet}) # Weighted Averages
    
    results.update({"wshannon_avg": aggregate_weighted_tiebreaker(group)}) # Weighted Shannon Average
    
    wwhp_lor = aggregate_weighted_lor(group, protein, hp_map)
    results.update({"wwhp_lor."+x: wwhp_lor[x] for x in hp_map}) # Weighted HP Log-Odds Ratio
    
    wwsz_lor = aggregate_weighted_lor(group, protein, sz_map)
    results.update({"wwsz_lor."+x: wwsz_lor[x] for x in sz_map}) # Weighted Size Log-Odds Ratio
    
    if all:
        hp_lor = aggregate_lor(group, protein, hp_map)
        results.update({"hp_lor."+x: hp_lor[x] for x in hp_map}) # Unweighted HP Log-Odds Ratio
        
        sz_lor = aggregate_lor(group, protein, sz_map)
        results.update({"sz_lor."+x: sz_lor[x] for x in sz_map}) # Unweighted Size Log-Odds Ratio
        
        sz_perc = aggregate_mean(group, sz_map)
        results.update({"sz_perc."+x: sz_perc[x] for x in sz_map}) # Unweighted HP Percentage
        
        hp_perc = aggregate_mean(group, hp_map)
        results.update({"hp_perc."+x: hp_perc[x] for x in hp_map}) # Unweighted Size Percentage
        
        wsz_perc = aggregate_weighted_mean(group, sz_map)
        results.update({"wsz_perc."+x: wsz_perc[x] for x in sz_map}) # Weighted HP Percentage
        
        whp_perc = aggregate_weighted_mean(group, hp_map)
        results.update({"whp_perc."+x: whp_perc[x] for x in hp_map}) # Weighted Size Percentage
        
        aa_perc = aggregate_mean(group, identity_map)
        results.update({"aa_perc."+x: aa_perc[x] for x in alphabet}) # Unweighted Averages
        
        results.update({"shannon_avg": aggregate_tiebreaker(group)}) # Unweighted Shannon Average
    return results

def read_select(aaseq, filename, binary=False):
    with open(filename, "r") as f:
        classes = {"{res}".format(res=res): "0" for res in aaseq}
        metals = ["Ca","Cu","Fe","Mg","Mn","Zn"]
        for line in f:
            if ">" in line: continue
            if len(line) < 1: continue
            res,metal = line.replace("\n","").split()
            if binary:
                metal = 1
            classes["{res}".format(res=res)] = metal
    return classes

def display(result):
    if isinstance(result, float):
        return "%0.3f" % result
    if isinstance(result, int):
        return "%i" % result
    elif isinstance(result, str):
        return result

def output_tsv(results, aaseq, i, args, delim="\t", first=False):
    classes = {}
    if args.annotation:
        classes = read_select(aaseq, args.annotation)
    keys = list(results.keys())
    keys.sort()
    if first:
        args.output.write(delim.join(["res","class"])+"\t")
        args.output.write(delim.join(keys)+"\n")
    name = ""
    if args.name:
        name = name + "_"
    args.output.write(delim.join([name+aaseq[i],str(classes.get(aaseq[i],"0"))])+"\t")
    args.output.write(delim.join([display(results[k]) for k in keys])+"\n")
    
def output(results, aaseq, i, args, first=False):
    if args.format == "tsv":
        output_tsv(results, aaseq, i, args, first=first)

def main(args):
    coevf = args.coev
    
    pssm = json.load(args.pssm)
    norm = {}
    if args.norm:
        norm = json.load(args.norm)
    
    aa = next(coevf).lstrip().split()
    seq = next(coevf).lstrip().split()
    aaseq = [aa[i]+seq[i] for i in range(len(aa))]
    shannon = [pssm[seqid]["Entropy"] for seqid in aaseq]
    
    i = 0
    first = True
    for row in coevf:
        if aa[i] not in args.subset:
            i += 1
            continue
        row = [float(r) for r in row.split()[2:]]
        results = features(row, shannon, aa, i, args.neighbors, pssm[aaseq[i]], args.subset, all=args.all)
        for k in results:
            if k in norm:
                results[k] = (float(results[k]) - float(norm[k]["mean"])) / float(norm[k]["sd"])
        output(results, aaseq, i, args, first=first)
        i += 1
        first = False
    
    args.output.close()
    if args.norm:
        args.norm.close()
    args.coev.close()
    args.pssm.close()
    
if __name__ == "__main__":
    ap = argparse.ArgumentParser("Generates Feature vectors from coevolution matrix.")
    ap.add_argument("--coev","-c", help="coevolution matrix file name",
                    type=argparse.FileType("r"), metavar="protein.align7.pearson.wph",
                    required=True)
    
    ap.add_argument("--pssm","-p", help="position-specific scoring matrix json file name",
                    type=argparse.FileType("r"), required=True, metavar="protein.pssm.json")
    
    ap.add_argument("--norm", "-N", help="reference normalization statistics (mean, sd) filename",
                    type=argparse.FileType("r"), required=False, default=None, metavar="norm_stats.json")
    
    ap.add_argument("--annotation","-A", help="annotation file",
                    type=str, required=False, default=None, metavar="annotation.select")
    
    ap.add_argument("--neighbors","-n", help="number of neighbors to consider",
                    type=int, required=False, default=4, metavar="#")
    
    ap.add_argument("--subset", "-S", help="amino acid subset to consider, no delimiter, one letter codes", default="CDEHNQST", type=str)
    
    ap.add_argument("--format", "-f", help="output format", choices=["arff","libsvm","tsv","csv"], default="tsv", type=str, required=False)
    
    ap.add_argument("--output","-o",help="output file name", required=False,
                    type=argparse.FileType("w"), metavar="protein.outputfmt", default=sys.stdout)
    
    ap.add_argument("--name", help="ID for use in residue labels", type=str, required=False, default=None, metavar="protein_name")
    
    ap.add_argument("--all","-a",help="generate all features (not just selected ones for metal binding)", default=False, action="store_const", const=True, dest="all")
    
    args = ap.parse_args()
    
    main(args)