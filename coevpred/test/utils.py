#!/usr/bin/env python3
import unittest
from coevpred.utils import *
from coevpred.utils.phi import *

class TopTest(unittest.TestCase):
    def test_top_correctness_list(self):
        x = [0.0,0.2,0.9,0.3,0.4,0.5]
        y = ["N","Y","X","Y","Y","Y"]
        tie = [0.4,0.4,0.4,0.4,0.4,0.4,0.4]
        y_top = [t.label for t in top_residues(x, 2, y, 4, tie)]
        self.assertNotIn("N",y_top)
        self.assertNotIn("X",y_top)
        self.assertEqual(len(y_top), 4)

    def test_top_correctness_blank(self):
        x = []
        y = []
        tie = []
        y_top = [t.label for t in top_residues(x, 2, y, 4, tie)]
        self.assertEqual(len(y_top), 0)
    
    def test_top_correctness_zero(self):
        x = [0.0,0.0,0.0,0.0,0.0,0.0]
        y = ["Y","Y","X","Y","Y","Y"]
        tie = [0.4,0.4,0.4,0.4,0.4,0.4]
        y_top = [t.label for t in top_residues(x, 2, y, 4, tie)]
        self.assertNotIn("N",y_top)
        self.assertNotIn("X",y_top)
        self.assertEqual(len(y_top), 5)
        
    def test_top_correctness_starve(self):
        x = ["0.3","0.2","0.4","0.55"]
        y = ["Y","Y","X","Y"]
        tie = [0.4,0.4,0.4,0.4,0.4,0.4,0.4]
        y_top = [t.label for t in top_residues(x, 2, y, 4, tie)]
        self.assertNotIn("X",y_top)
        self.assertEqual(len(y_top), 3)
    
    def test_top_correctness_tiebreak(self):
        x = [0.2,0.2,0.2,0.2,0.2,0.2,0.1]
        y = ["N","Y","X","Y","Y","Y","N"]
        tie = [0.1,0.4,0.3,0.6,0.8,0.7,0.9]
        y_top = [t.label for t in top_residues(x, 2, y, 4, tie)]
        self.assertNotIn("X",y_top)
        self.assertNotIn("N",y_top)
        self.assertEqual(len(y_top), 4)
    
    def test_top_correctness_tie(self):
        x = [0.2,0.2,0.2,0.2,0.2,0.2,0.1]
        y = ["Y","Y","X","Y","Y","Y","N"]
        tie = [0.4,0.4,0.4,0.4,0.4,0.4,0.4]
        y_top = [t.label for t in top_residues(x, 2, y, 4, tie)]
        self.assertNotIn("X",y_top)
        self.assertNotIn("N",y_top)
        self.assertEqual(len(y_top), 5)
    
    def test_top_correctness_tie2(self):
        x = [0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.1]
        y = ["Y","Y","X","Y","Y","Y","Y","N"]
        tie = [0.4,0.4,0.4,0.4,0.4,0.4,0.4,0.4]
        y_top = [t.label for t in top_residues(x, 2, y, 4, tie)]
        self.assertNotIn("X",y_top)
        self.assertNotIn("N",y_top)
        self.assertEqual(len(y_top), 6)
    
    def test_data_zero(self):
        x = []
        y = []
        tie = []
        y_data = residue_data(x, 0, y, tie)
        self.assertEqual(len(y_data), 0)
    
    def test_data_full(self):
        x = [0.9, 0.2, 0.3, 0.4, 0.5, 0.2, 0.1, 0.4, 0.5]
        y = ["X", "Y", "Y", "Y", "Y", "A", "B", "Y", "Y"]
        tie = [0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4]
        y_data = residue_data(x, 0, y, tie)
        y_data_labels = [t.label for t in y_data]
        self.assertNotIn("X",y_data_labels)
        self.assertIn("Y",y_data_labels)
        self.assertEqual(len(y_data), 8)

class PhiTest(unittest.TestCase):
    def test_phi_no_diff(self):
        x = [0,0,1,1,0.245,0.743,0]
        self.assertEqual(phi_pop(x,x),0)
    
    def test_phi_diff(self):
        x = [0,0,1,1,0.245,0.743,0]
        y = x + [0]
        self.assertNotEqual(phi_pop(x,y),0)
        self.assertEqual(phi_pop(x,y),phi(y,x))
        self.assertGreater(phi_pop(x,y),0)
        
if __name__=="__main__":
    unittest.main()